import React, { Component } from 'react';
import axios from 'axios';
import {List, ListItem} from 'material-ui/List';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import SimpleMDE from 'react-simplemde-editor';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import 'react-simplemde-editor/dist/simplemde.min.css';

export default class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: undefined,
      files: {},
      data: '',
      open: false
    }
  }
  componentWillMount() {
    return axios.get('http://localhost:4040/api/file')
      .then(res => this.setState({files: res.data}));
  }

  selectItem = (file) => {
    this.setState({current: file});
    this.setState({data: this.state.files[file]});
  }

  saveItem = () => axios
                    .put(`http://localhost:4040/api/file/${this.state.current}`, { data: this.state.data })
                    .then(() => this.setState({open: true}))
                    .then(() => axios.get('http://localhost:4040/api/file'))
                    .then(res => this.setState({files: res.data}));

  handleChange = (data) => this.setState({data})

  handleActionTouchTap = () => this.setState({open: true})

  handleRequestClose = () => this.setState({open: false})

  render() {
    return (
      <MuiThemeProvider>
        <div style={{marginTop: '100px'}}>
          <h1>FAQ Editor</h1>
          <List style={{width: '30%'}}>
            {Object.keys(this.state.files).map(file => (
              <ListItem
                primaryText={file}
                onClick={() => this.selectItem(file)}
                rightIcon={this.state.current === file ? <ActionGrade /> : ''}
              />
            ))}
          </List>
          {this.state.current ? <div style={{width: '60%', float: 'right', marginTop: '-100px', marginRight: '100px'}}>
            <SimpleMDE
              onChange={this.handleChange}
              value={this.state.data}
            />
            <FlatButton label="Save" onClick={this.saveItem} />
          </div> : ''}
          <Snackbar
            open={this.state.open}
            message="Saved Doc"
            autoHideDuration={2000}
            onActionTouchTap={this.handleActionTouchTap}
            onRequestClose={this.handleRequestClose}
          />
        </div>
      </MuiThemeProvider>
    );
  }
}
